﻿using UnityEngine;
using System.Collections;

public class TreeMaybe : MonoBehaviour {

	public GameObject tree;
	Vector3 pos;
	Quaternion rot;
	private int treeAmount;
	public Transform viewer;
	public GameObject viewerObj;

	public float view1;
	public float view2;
	public float view3;

	public float chunk1;
	public float chunk2;
	public float chunk3;

	public Vector3 distance;
	public bool hasSpawned;





	void Start () {

		rot = new Quaternion (0, 0, 0,0);
		pos = this.transform.localPosition + new Vector3 (0, 150, 0);
		tree = Resources.Load ("tree 1") as GameObject;

		treeAmount = Random.Range (0,10);


	
	}
	

	void Update () {

		if (this.hasSpawned != true) {
			this.hasSpawned = false;
		}

		viewerObj = GameObject.FindGameObjectWithTag ("viewer");
		viewer = viewerObj.transform;

		view1 = viewer.position.x;
		view2 = viewer.position.z;
		view3 = viewer.position.y;

		chunk1 = this.transform.position.x;
		chunk2 = this.transform.position.z;
		chunk3 = this.transform.position.y;

		distance = new Vector3 (chunk1 - view1, chunk3 - view3, chunk2 - view2);

		if (distance.x <= 108 &&
		    distance.x >= -108 &&
		    distance.z <= 108 &&
		    distance.z >= -108 &&
		       this.hasSpawned == false) {

			   this.hasSpawned = true;

				for (int i = 0; i < treeAmount; i++) {
				Instantiate (tree, pos, rot);
						}
					}
				}
			}
		
	


