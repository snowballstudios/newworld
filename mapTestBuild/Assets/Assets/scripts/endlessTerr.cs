﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class endlessTerr : MonoBehaviour {

	const float viewerMoveThresh4ChunkUp = 25f;
	const float sqrviewerMoveThresh4ChunkUp = viewerMoveThresh4ChunkUp * viewerMoveThresh4ChunkUp;
	public static float maxViewDst;
	public LODInfo[] detailLevels;


	const float scale = 1f;

	public Transform viewer;
	public Material mapMat;

	public static Vector2 viewerPos;
	Vector2 viewerPosOld;
	public static mapGen mapGenerator;

	int chunkSize;
	int chunkVisInViewDst;




	// DICTIONARY IS HERE
	Dictionary <Vector2, TerrainChunk> terrainChunkDict = new Dictionary <Vector2, TerrainChunk> ();
	static List <TerrainChunk> terrVisLastUp = new List <TerrainChunk>();

	//START IS HERE
	void Start () {
		mapGenerator = FindObjectOfType<mapGen> ();

		maxViewDst = detailLevels [detailLevels.Length - 1].visDistThres;
		chunkSize = mapGen.mapChunkSize - 1;
		chunkVisInViewDst =  Mathf.RoundToInt(maxViewDst) / chunkSize;

		UpdateVisChunks ();

}
	//UPDATE IS HERE
	void Update () {
		viewerPos = new Vector2 (viewer.position.x, viewer.position.z) / scale;

		if ((viewerPosOld - viewerPos).sqrMagnitude > sqrviewerMoveThresh4ChunkUp) {
			viewerPosOld = viewerPos;
			UpdateVisChunks ();
		}
	}
	//VISABLE CHUNKS MATE
	void UpdateVisChunks() {

		for (int i = 0; i < terrVisLastUp.Count; i++) {
			terrVisLastUp [i].SetVis (false);
		}

		terrVisLastUp.Clear ();

		int currentchunkCoX = Mathf.RoundToInt (viewerPos.x / chunkSize);
		int currentchunkCoY = Mathf.RoundToInt (viewerPos.y / chunkSize);

		for (int yOffset = - chunkVisInViewDst; yOffset <= chunkVisInViewDst; yOffset++) {
			for (int xOffset = - chunkVisInViewDst; xOffset <= chunkVisInViewDst; xOffset++) {
				Vector2 viewedChunkCo = new Vector2 (currentchunkCoX + xOffset, currentchunkCoY + yOffset);

				if (terrainChunkDict.ContainsKey (viewedChunkCo)) {
					terrainChunkDict[viewedChunkCo].UpdateTerrainChunk ();

				} else {
					terrainChunkDict.Add (viewedChunkCo, new TerrainChunk (viewedChunkCo, chunkSize, detailLevels, transform, mapMat));

				}
			}

		}
	}
	//ALL OF YOUR TERRAIN CHUNKS ARE BELONG TO US
	public class TerrainChunk {

		GameObject meshObject;
		Vector2 position;
		Bounds bounds;


	
		MeshRenderer meshRenderer;
		MeshFilter meshFilter;
		MeshCollider meshCollider;


		LODInfo[] detailLevels;
		LODMesh[] lodMeshes;
		LODMesh collisionLODMesh;

		MapData mapData;
		bool mapDataRec;

		int prevLodIndex = -1;


		// END OF TERRAIN CHUNK VARIABLES
		public TerrainChunk(Vector2 coord, int size, LODInfo[] detailLevels, Transform parent, Material material) {
			this.detailLevels = detailLevels;
			position = coord * size;
			bounds = new Bounds (position, Vector2.one * size);
			Vector3 positionV3 = new Vector3(position.x, 0 , position.y);

			meshObject = new GameObject("TerrainChunk");
			meshRenderer = meshObject.AddComponent<MeshRenderer>();
			meshFilter = meshObject.AddComponent<MeshFilter>();
			meshCollider = meshObject.AddComponent<MeshCollider>();
			meshRenderer.material = material;
			meshObject.AddComponent<TreeMaybe>();
			
			



			meshObject.transform.position = positionV3 * scale;
			meshObject.transform.parent = parent;
			meshObject.transform.localScale = Vector3.one * scale;
			SetVis (false);

			lodMeshes = new LODMesh[detailLevels.Length];
			for (int i = 0; i < detailLevels.Length; i++) {
				lodMeshes[i] = new LODMesh(detailLevels[i].lod, UpdateTerrainChunk);
				if (detailLevels[i].useForCollider) {
					collisionLODMesh = lodMeshes[i];
				
			}
			}

			mapGenerator.RequestMapData(position, OnMapDataRecieved);



		}
		// ON MAP DATA RECIEVED = BUM SEX ;)
		void OnMapDataRecieved (MapData mapData) {
			this.mapData = mapData;
			mapDataRec = true;

			Texture2D texture = texGen.TexFromColMap (mapData.colourMap, mapGen.mapChunkSize, mapGen.mapChunkSize);
			meshRenderer.material.mainTexture = texture;
			UpdateTerrainChunk ();

		}


		//THIS IS WERE WE UPDATE THE TERRAIN CHUNKS
		public void UpdateTerrainChunk () {
		




			if (mapDataRec) {
					float viewerDstFromNearEdge = Mathf.Sqrt (bounds.SqrDistance (viewerPos));
					bool visible = viewerDstFromNearEdge <= maxViewDst;

					if (visible) {
						int lodIndex = 0;

					
						for (int i =0; i < detailLevels.Length - 1; i++) {
							if (viewerDstFromNearEdge > detailLevels [i].visDistThres) {
								lodIndex = i + 1;
							} else { //MUCH ELSE, SUCH BREAK
								break;
							}
						}
						if (lodIndex != prevLodIndex) {
							LODMesh lodMesh = lodMeshes [lodIndex];
							if (lodMesh.hasMesh) {
								prevLodIndex = lodIndex;
								meshFilter.mesh = lodMesh.mesh;
							} else if (!lodMesh.hasRequestedMesh) {
								lodMesh.RequestMesh (mapData);
							}
						}

					if (lodIndex == 0) {



						if( collisionLODMesh.hasMesh) {
							meshCollider.sharedMesh = collisionLODMesh.mesh;

						} else if (!collisionLODMesh.hasRequestedMesh) {
							collisionLODMesh.RequestMesh(mapData);
						}
					}


					terrVisLastUp.Add (this);

					}

					SetVis (visible);
			}
		}

		public void SetVis (bool visible) {
			meshObject.SetActive (visible);
		}

		public bool IsVis() {
			return meshObject.activeSelf;
		}

	}

	class LODMesh  {

		public Mesh mesh;
		public bool hasRequestedMesh;
		public bool hasMesh;
		int lod;
		System.Action updateCallback;

		public LODMesh (int lod, System.Action updateCallback){
			this.lod = lod;
			this.updateCallback = updateCallback;
		}

		void OnMeshDataReceived (MeshData meshData) {
			mesh = meshData.CreateMesh ();
			hasMesh = true;

			updateCallback ();
		}

		public void RequestMesh(MapData mapData){
			hasRequestedMesh = true;
			mapGenerator.RequestMeshData (mapData, lod, OnMeshDataReceived);
		}

	}
	[System.Serializable]
	public struct LODInfo  {

		public int lod;
		public float visDistThres;
		public bool useForCollider;


	}

}
