﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;
using System.Collections.Generic;

public class mapGen : MonoBehaviour {

	public enum DrawMode {NoiseMap, ColourMap, Mesh, FallOffMap};
	public DrawMode drawMode;

	public noise.NormalizeMode normaliseMode;

	 public const int mapChunkSize = 105;
	//public const int mapChunkSize = 239;

	public bool useFlatShade;
	[Range(0,6)]
	public int prevLvlOfDetail;
	public float noiseScale;

	public int octaves;
	[Range(0,1)]
	public float persistance;
	public float lacunarity;
	public int seed;
	public Vector2 offset;

	public bool useFallOff;

	public float meshHeightMult;
	public AnimationCurve meshHeightCurve;

	public bool autoUp;

	public TerrainType[] regions;

	float[,] fallOffMap;

	Queue<MapThreadInfo<MapData>> mapDataThreadInfoQu = new Queue<MapThreadInfo<MapData>>();
	Queue<MapThreadInfo<MeshData>> meshDataThreadInfoQu = new Queue<MapThreadInfo<MeshData>>();

	void Awake() {
		fallOffMap = fallOffGen.GenFalloffMap (mapChunkSize);
		seed  = UnityEngine.Random.Range(0,60000);
	}


	public void DrawMapInEdit () {
		MapData mapData = GenerateMapData (Vector2.zero); 
		mapDisplay display = FindObjectOfType<mapDisplay> ();
		if (drawMode == DrawMode.NoiseMap) {
			display.DrawTexture (texGen.TexFromHiMap (mapData.heightMap));
		} else if (drawMode == DrawMode.ColourMap) {
			display.DrawTexture (texGen.TexFromColMap (mapData.colourMap, mapChunkSize, mapChunkSize));
		} else if (drawMode == DrawMode.Mesh) {
			display.DrawMesh (meshGen.GenTerrMesh (mapData.heightMap, meshHeightMult, meshHeightCurve, prevLvlOfDetail, useFlatShade), texGen.TexFromColMap (mapData.colourMap, mapChunkSize, mapChunkSize));
		} else if (drawMode == DrawMode.FallOffMap) {
			display.DrawTexture(texGen.TexFromHiMap(fallOffGen.GenFalloffMap(mapChunkSize)));
		}
	}


	public void RequestMapData (Vector2 centre, Action<MapData> callBack) {
		ThreadStart threadStart = delegate {
			MapDataThread (centre, callBack);
		};
 
		new Thread (threadStart).Start ();

	}

	void MapDataThread (Vector2 centre, Action<MapData> callBack) {
		MapData mapData = GenerateMapData (centre);
		lock (mapDataThreadInfoQu) {
			mapDataThreadInfoQu.Enqueue (new MapThreadInfo<MapData> (callBack, mapData)); 
		}
	} 

	public void RequestMeshData(MapData mapData,int lod, Action<MeshData> callBack) {
		ThreadStart threadStart = delegate {
			MeshDataThread(mapData, lod, callBack);
		};
		new Thread(threadStart).Start();
	}

	void MeshDataThread (MapData mapData, int lod, Action<MeshData> callBack) {
		MeshData meshData = meshGen.GenTerrMesh (mapData.heightMap, meshHeightMult, meshHeightCurve, lod, useFlatShade);
		lock (meshDataThreadInfoQu) {
			meshDataThreadInfoQu.Enqueue (new MapThreadInfo<MeshData> (callBack, meshData));

		}
	}
	

	void Update () {
		if (mapDataThreadInfoQu.Count > 0) {
			for (int i = 0; i < mapDataThreadInfoQu.Count; i ++) {
				MapThreadInfo<MapData> threadInfo = mapDataThreadInfoQu.Dequeue ();
				threadInfo.callBack(threadInfo.parameter);
			}
		}
		if (meshDataThreadInfoQu.Count > 0) {
			for(int i = 0; i < meshDataThreadInfoQu.Count; i++) {
				MapThreadInfo<MeshData> ThreadInfo = meshDataThreadInfoQu.Dequeue ();
				ThreadInfo.callBack (ThreadInfo.parameter);

	        }
		}
	}
	

	MapData GenerateMapData(Vector2 centre) {
		float [,] noiseMap = noise.GenNoiseMap (mapChunkSize + 2, mapChunkSize + 2, seed, noiseScale, octaves, persistance, lacunarity,centre + offset, normaliseMode);
		Color[] colourMap = new Color[mapChunkSize * mapChunkSize];
		for (int y = 0; y < mapChunkSize; y++) {
			for (int x = 0; x < mapChunkSize; x++) {
				if(useFallOff) {
					noiseMap[x,y] = Mathf.Clamp01(noiseMap[x,y] - fallOffMap [x,y]);
				}


				float currentHeight = noiseMap [x, y];
				for (int i = 0; i < regions.Length; i++) {
					if (currentHeight >= regions [i].height) {
						colourMap [y * mapChunkSize + x] = regions [i].colour;
					} else {
						break;
					}
				}
			}
		}
		return new MapData (noiseMap, colourMap);
                    
	}
	void OnValidate() {

		if (lacunarity < 1) {
			lacunarity = 1;
		}
		if (octaves < 0) {
			octaves = 0;
		}

		fallOffMap = fallOffGen.GenFalloffMap (mapChunkSize);

	}

	struct MapThreadInfo<T> {
		public readonly Action<T> callBack;
		public readonly T parameter;

		public MapThreadInfo (Action<T> callBack, T parameter)
		{
			this.callBack = callBack;
			this.parameter = parameter;
		}

	}

}
 [System.Serializable]
public struct TerrainType {

	public string name;
	public float height;
	public Color colour;

}

public struct MapData {
	public readonly float [,] heightMap;
	public readonly Color[] colourMap;

	public MapData (float[,] heightmap, Color[] colourMap)
	{
		this.heightMap = heightmap;
		this.colourMap = colourMap;
	}
}