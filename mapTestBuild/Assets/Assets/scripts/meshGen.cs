﻿using UnityEngine;
using System.Collections;

public static class meshGen  {

	public static MeshData GenTerrMesh(float[,] heightMap,float heightMult, AnimationCurve heightCurve, int lvlOfDetail, bool useFlatShade){
		//AnimationCurve heightCurve2 = new AnimationCurve (heightCurve.keys);

		int meshSimpleInc = (lvlOfDetail ==0)?1: lvlOfDetail * 2;
		int borderedSize = heightMap.GetLength (0);
		int meshSize = borderedSize - 2 * meshSimpleInc;
		int meshSizeUnsimple = borderedSize - 2;
	
		float topLeftX = (meshSizeUnsimple - 1) / -2f;
		float topLeftZ = (meshSizeUnsimple - 1) / 2f;


		int vertPerLine = (meshSize - 1) / meshSimpleInc + 1;


		MeshData meshData = new MeshData (vertPerLine, useFlatShade);

		int[,] vertIndMap = new int[borderedSize,borderedSize];
		int meshVertInd = 0;
		int borderedVertInd = -1;

		for (int y = 0; y < borderedSize; y += meshSimpleInc) {
			for (int x = 0; x < borderedSize; x += meshSimpleInc) {
				bool isBorderVert = y == 0 || y == borderedSize -1 || x == 0 || x == borderedSize -1;
			if (isBorderVert) {
					vertIndMap [x,y] = borderedVertInd;
					borderedVertInd--;
			     } else {
					vertIndMap[x,y] = meshVertInd;
					meshVertInd++;
				}
			}
		}

		for (int y = 0; y < borderedSize; y += meshSimpleInc) {
			for (int x = 0; x < borderedSize; x += meshSimpleInc) {

				int vertInd = vertIndMap[x,y];
			

			lock (heightCurve) {
					Vector2 percent = new Vector2((x- meshSimpleInc)/(float)meshSize , (y - meshSimpleInc)/ (float)meshSize);
					float height = heightCurve.Evaluate(heightMap [x,y]) * heightMult;
				Vector3 vertPos  = new Vector3 (topLeftX + percent.x * meshSizeUnsimple, height, topLeftZ - percent.y * meshSizeUnsimple);
					meshData.AddVertex(vertPos, percent, vertInd);
				}

				if (x < borderedSize-1 && y < borderedSize - 1) {
					int a = vertIndMap [x,y];
					int b = vertIndMap [x + meshSimpleInc,y];
					int c = vertIndMap [x,y + meshSimpleInc];
					int d = vertIndMap [x + meshSimpleInc,y + meshSimpleInc];

					meshData.AddTriangle (a, d, c);
					meshData.AddTriangle (d, a, b);
				}

				vertInd++;
			}
		}

		meshData.ProcessMesh();

		return meshData;
	}

}


public class MeshData {
	 Vector3[] vertices;
	 int[] triangles;
	 Vector2[] uvs;
	Vector3[] bakedNorms;

	Vector3[] borderVerts;
	int[] borderTris;


	int triangleIndex;
	int borderTriInd;

	bool useFlatShade; 

	public MeshData(int vertPerLine, bool useFlatShade) {
		this.useFlatShade = useFlatShade;

		vertices = new Vector3[vertPerLine * vertPerLine];
		uvs = new Vector2[vertPerLine * vertPerLine];
		triangles = new int[(vertPerLine-1) * (vertPerLine-1) * 6];

		borderVerts = new Vector3[vertPerLine * 4 + 4];
		borderTris = new int[24 * vertPerLine];


	}

	public void AddVertex (Vector3 vertPos, Vector2 uv, int vertInd) {
		if (vertInd < 0) {
			borderVerts[-vertInd-1] = vertPos;


		} else {
			vertices[vertInd] = vertPos;
			uvs [vertInd] = uv;
		}
	}

	public void AddTriangle (int a, int b, int c) {
		if (a < 0 || b < 0 || c < 0) {
			borderTris [borderTriInd] = a;
			borderTris [borderTriInd + 1] = b;
			borderTris [borderTriInd + 2] = c;
			borderTriInd += 3;

		}
		else {
			triangles [triangleIndex] = a;
			triangles [triangleIndex + 1] = b;
			triangles [triangleIndex + 2] = c;
			triangleIndex += 3;
		}
	}
	Vector3[]calcNorms() {
		
		Vector3[] vertexNorms = new Vector3[vertices.Length];
		int triCount = triangles.Length / 3;
		for (int i = 0; i < triCount; i++) {
			int normTriInd = i * 3;
			int vertIndA = triangles [normTriInd];
			int vertIndB = triangles [normTriInd + 1];
			int vertIndC = triangles [normTriInd + 2];

			Vector3 triNorm = SurfaceNormalFromIndices (vertIndA, vertIndB, vertIndC);
			vertexNorms [vertIndA] += triNorm;
			vertexNorms [vertIndB] += triNorm;
			vertexNorms [vertIndC] += triNorm;
		}
		int bordertriCount = borderTris.Length / 3;
		for (int i = 0; i < bordertriCount; i++) {
			int normTriInd = i * 3;
			int vertIndA = borderTris [normTriInd];
			int vertIndB = borderTris [normTriInd + 1];
			int vertIndC = borderTris [normTriInd + 2];
			
			Vector3 triNorm = SurfaceNormalFromIndices (vertIndA, vertIndB, vertIndC);
			if (vertIndA >= 0) {
			vertexNorms [vertIndA] += triNorm;
			}
			if (vertIndB >= 0) {
			vertexNorms [vertIndB] += triNorm;
			}
			if (vertIndC >= 0) {
			vertexNorms [vertIndC] += triNorm;
			}	
		}


		for (int i =0; i < vertexNorms.Length; i++) {
			vertexNorms [i].Normalize ();
		}
		return vertexNorms;
	}

	Vector3 SurfaceNormalFromIndices(int indexA, int indexB, int indexC) {
		Vector3 pointA = (indexA < 0 )?borderVerts[-indexA-1] : vertices [indexA];
		Vector3 pointB = (indexB < 0 )?borderVerts[-indexB-1] : vertices [indexB];
		Vector3 pointC = (indexC < 0 )?borderVerts[-indexC-1] : vertices [indexC];

		Vector3 sideAB = pointB - pointA;
		Vector3 sideAC = pointC - pointA;
		return Vector3.Cross (sideAB, sideAC).normalized;


	}
	public void ProcessMesh() {
		if (useFlatShade) {
			FlatShading ();
		} else {
			BakedNorms ();
		}
	}
    void BakedNorms() {
		bakedNorms = calcNorms();
	}

	void FlatShading (){
		Vector3[] flatShadedVert = new Vector3[triangles.Length];
		Vector2[] flatShadedUvs = new Vector2[triangles.Length];

		for (int i = 0; i < triangles.Length; i++) {
			flatShadedVert [i] = vertices [triangles [i]];
			flatShadedUvs[i] = uvs [triangles[i]];
			triangles [i] = i;
		}

		vertices = flatShadedVert;
		uvs = flatShadedUvs;
	}



	public Mesh CreateMesh() {
		Mesh mesh = new Mesh ();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.uv = uvs;

		if (useFlatShade) {
			mesh.RecalculateNormals ();
		} else {
			mesh.normals = bakedNorms;
		}
		return mesh;
	}
  }

