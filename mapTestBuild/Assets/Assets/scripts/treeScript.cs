﻿using UnityEngine;
using System.Collections;

public class treeScript : MonoBehaviour {


	public bool terrainCollision = false;
	public Rigidbody rb;
	private Vector3 pos;
	private Quaternion rot;
	public GameObject parentObj;
	public Transform parent;

	public Transform viewer;
	public GameObject viewerObj;
	
	public float view1;
	public float view2;
	public float view3;
	
	public float chunk1;
	public float chunk2;
	public float posY;
	public Vector3 distance;
	public bool treeAct;

	public GameObject tree1;
	public GameObject tree2;
	public GameObject tree3;




	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		parentObj = GameObject.FindGameObjectWithTag ("mapGen");
		parent = parentObj.transform;

		this.transform.parent = parent;

		pos = this.transform.localPosition + new Vector3 
			(Random.Range(-50,50),0,Random.Range(-50,50));
		rot = new Quaternion (0,0,0,0);

		this.transform.position = pos;
		this.transform.rotation = rot;

		tree1 = this.transform.FindChild ("leaves").gameObject;
		tree2 = this.transform.FindChild ("leaves1").gameObject;
		tree3 = this.transform.FindChild ("tree").gameObject;

	}
	
	// Update is called once per frame
	void Update () {

	

		if (terrainCollision == true) {
			if(this.transform.position.y >= 8.4){
				Destroy (this.gameObject);
			}
			rb.constraints = RigidbodyConstraints.FreezeAll;
		}
		// viewerPos + position variables
		viewerObj = GameObject.FindGameObjectWithTag ("viewer");
		viewer = viewerObj.transform;
		
		view1 = viewer.position.x;
		view2 = viewer.position.z;
		view3 = viewer.position.y;
		
		chunk1 = this.transform.position.x;
		chunk2 = this.transform.position.z;
		posY = this.transform.position.y;
		
		distance = new Vector3 (chunk1 - view1, posY - view3, chunk2 - view2);




		RaycastHit hit;
		Vector3 down = transform.TransformDirection (Vector3.down) * 200;
		
		if (Physics.Raycast (transform.position, (down), out hit)) {
			this.transform.position = new Vector3 (chunk1, posY - hit.distance, chunk2);

			this.gameObject.layer = 9;
			tree1.layer = 9;
			tree2.layer = 9;
			tree3.layer = 9;

		}

	

	



	}

	void OnCollisionEnter(Collision c){
		terrainCollision = true;

	}
	void OnTriggerEnter(Collider o){
		if (o.gameObject.tag == "tree") {
			Destroy (this.gameObject);

		}
	}
}
