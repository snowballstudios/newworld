﻿using UnityEngine;
using System.Collections;
using System;

public class mapGenCave : MonoBehaviour {

	public int width;
	public int height;

	public string seed;

	public bool useRandSeed;

	[Range(0,100)]
	public int randFillPerc;

	int[,] map;

	void Start() {
		GenerateCaveMap ();
	
	}

	void Update(){
		if (Input.GetMouseButtonDown (0)) {
			GenerateCaveMap();
		}

	}

	void GenerateCaveMap(){

		int newI = 3;
		map = new int[width,height];
		RandomFillMap ();

		for (int i =0; i < newI; i ++) {
			SmoothMap();
		}

		meshGenCave meshGen = GetComponent<meshGenCave> ();
		meshGen.GenerateMesh (map, 1);
	}

	// Seeds and Random filling
	void RandomFillMap(){
		if (useRandSeed) {
			seed = Time.time.ToString();
		}
		// random number gen
		System.Random prng = new System.Random (seed.GetHashCode ());

		for (int x = 0; x < width; x ++) {
			for (int y = 0; y < height; y ++) {

				if (x == 0 || x == width-1 || y ==0 || y == height-1){
					map[x,y] = 1;

				} else {
				map[x,y] = (prng.Next(0,100) < randFillPerc)? 1: 0;

				}
			}
		}
	}
	//First great parsing wall
	void SmoothMap(){
		for (int x = 0; x < width; x ++) {
			for (int y = 0; y < height; y ++) {
				int neighWalltiles = GetSorroundWallCount(x,y);

				if (neighWalltiles > 4)
					map[x,y] = 1;
				else if(neighWalltiles < 4)
					map[x,y] = 0;

			}
		}
	}
	//Second great parsing wall

	int GetSorroundWallCount (int gridX, int gridY) {
		int wallCount = 0;
		for (int neighX = gridX - 1; neighX <= gridX + 1; neighX ++) {
			for (int neighY = gridY - 1; neighY <= gridY + 1; neighY ++) {

				if (neighX >= 0 && neighX < width && neighY >= 0 && neighY < height){
					if (neighX != gridX || neighY != gridY){
						wallCount += map[neighX,neighY];
					}
				}
				else{
					wallCount ++;
				}
			}
		}
		return wallCount;
	}
	//Third great parsing wall


	void OnDrawGizmos() {
		if (map != null) {
			for (int x = 0; x < width; x ++) {
				for (int y = 0; y < height; y ++) {

					Gizmos.color = (map[x,y] == 1)? Color.black: Color.white;

					Vector3 pos = new Vector3(-width/2 + x + 0.5f, 0, -height/ 2 + y + 0.5f);
					Gizmos.DrawCube(pos, Vector3.one);
				}
			}
		}
	}
}
//Fourth great parsing wall





