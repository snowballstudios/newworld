﻿using UnityEngine;
using System.Collections;

public class meshGenCave : MonoBehaviour {

	public SquareGrid squareGrid;

	public void GenerateMesh(int[,] map, float squareSize) {
		squareGrid = new SquareGrid (map, squareSize);
	}

	public class SquareGrid {
		public Square[,] squares;

		public SquareGrid	(int[,] map, float squareSize) {
			int nodeCountX = map.GetLength(0);
			int nodeCountY = map.GetLength(1);

			float mapWidth = nodeCountX * squareSize;
			float mapHeight = nodeCountY * squareSize;

			CNode[,] controlNodes = new CNode[nodeCountX,nodeCountY];

			for (int x = 0; x < nodeCountX; x++){
				for (int y = 0; y < nodeCountY; y++){

					Vector3 pos = new Vector3
	(-mapWidth/2 + x * squareSize + squareSize/2, 0,-mapHeight/2 + y *squareSize + squareSize/2);

					controlNodes[x,y] = new CNode (pos, map[x,y] == 1, squareSize);
				}
			}
			squares = new Square[nodeCountX -1, nodeCountY -1];
			for (int x = 0; x < nodeCountX -1; x++){
				for (int y = 0; y < nodeCountY -1; y++){

					squares[x,y] = new Square
	(controlNodes[x,y+1], controlNodes[x+1,y+1], controlNodes[x+1,y], controlNodes[x,y]);				
					
			}

		}
	}
}

	public class Square {

		public CNode topLeft, topRight, bottomRight, bottomLeft;
		public Node centreTop, centreRight, centreBottom, centreLeft;

		public Square (CNode _topLeft, CNode _topRight, CNode _bottomLeft, CNode _bottomRight) {
			topLeft = _topLeft;
			topRight = _topRight;
			bottomLeft = _bottomLeft;
			bottomRight = _bottomRight;

			centreTop = topLeft.right;
			centreRight = bottomRight.above;
			centreBottom = bottomLeft.right;
			centreLeft = bottomLeft.above;
			

	    }
	}

		public class Node {
		public Vector3 position;
		public int vertexInd = -1;

		public Node(Vector3 _pos) {
			position = _pos;
		}
	}
	public class CNode : Node {

		public bool active;
		public Node above, right;

		public CNode( Vector3 _pos,bool _active, float squareSize) : base(_pos) {

			active = _active;

			above = new Node(position + Vector3.forward * squareSize/2f);
			right = new Node(position + Vector3.right * squareSize/2f);
		}

	}
}
