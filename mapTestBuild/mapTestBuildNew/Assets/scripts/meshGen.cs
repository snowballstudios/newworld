﻿using UnityEngine;
using System.Collections;

public static class meshGen  {

	public static MeshData GenTerrMesh(float[,] heightMap,float heightMult, AnimationCurve heightCurve, int lvlOfDetail){
		//AnimationCurve heightCurve2 = new AnimationCurve (heightCurve.keys);

		int width = heightMap.GetLength (0);
		int height = heightMap.GetLength (1);
		float topLeftX = (width - 1) / -2f;
		float topLeftZ = (height - 1) / 2f;

		int meshSimpleInc = (lvlOfDetail ==0)?1: lvlOfDetail * 2;
		int vertPerLine = (width - 1) / meshSimpleInc + 1;


		MeshData meshData = new MeshData (vertPerLine, vertPerLine);

		int vertInd = 0;

		for (int y = 0; y < height; y += meshSimpleInc) {
			for (int x = 0; x < width; x += meshSimpleInc) {
			
			lock (heightCurve) {
				meshData.vertices[vertInd] = new Vector3 (topLeftX + x, heightCurve.Evaluate(heightMap [x,y]) * heightMult, topLeftZ - y);
				}
				meshData.uvs[vertInd] = new Vector2(x/(float)width, y/ (float)height);

				if (x < width-1 && y < height - 1) {
					meshData.AddTriangle (vertInd, vertInd + vertPerLine + 1, vertInd + vertPerLine);
					meshData.AddTriangle (vertInd + vertPerLine + 1,vertInd , vertInd + 1);
				}

				vertInd++;
			}
		}

		return meshData;
	}

}


public class MeshData {
	public Vector3[] vertices;
	public int[] triangles;
	public Vector2[] uvs;

	int triangleIndex;

	public MeshData(int meshWidth, int meshHeight) {
		vertices = new Vector3[meshWidth * meshHeight];
		uvs = new Vector2[meshWidth * meshHeight];
		triangles = new int[(meshWidth-1) * (meshHeight-1) * 6];


	}

	public void AddTriangle (int a, int b, int c) {
		triangles [triangleIndex] = a;
		triangles [triangleIndex + 1] = b;
		triangles [triangleIndex + 2] = c;
		triangleIndex += 3;
	}
	Vector3[]calcNorms() {
		
		Vector3[] vertexNorms = new Vector3[vertices.Length];
		int triCount = triangles.Length / 3;
		for (int i = 0; i < triCount; i++) {
			int normTriInd = i * 3;
			int vertIndA = triangles [normTriInd];
			int vertIndB = triangles [normTriInd + 1];
			int vertIndC = triangles [normTriInd + 2];

			Vector3 triNorm = SurfaceNormalFromIndices (vertIndA, vertIndB, vertIndC);
			vertexNorms [vertIndA] += triNorm;
			vertexNorms [vertIndB] += triNorm;
			vertexNorms [vertIndC] += triNorm;
		}
		for (int i =0; i < vertexNorms.Length; i++) {
			vertexNorms [i].Normalize ();
		}
		return vertexNorms;
	}

	Vector3 SurfaceNormalFromIndices(int indexA, int indexB, int indexC) {
		Vector3 pointA = vertices [indexA];
		Vector3 pointB = vertices [indexB];
		Vector3 pointC = vertices [indexC];

		Vector3 sideAB = pointB - pointA;
		Vector3 sideAC = pointC - pointA;
		return Vector3.Cross (sideAB, sideAC).normalized;


	}

	public Mesh CreateMesh() {
		Mesh mesh = new Mesh ();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		mesh.normals = calcNorms ();
		return mesh;
	}
}


















	
