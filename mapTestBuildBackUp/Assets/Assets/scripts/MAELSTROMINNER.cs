﻿using UnityEngine;
using System.Collections;

public class MAELSTROMINNER : MonoBehaviour {
	public float rotSpeed = 500;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		this.transform.Rotate (Vector3.forward * Time.deltaTime * rotSpeed);
	}
}
