﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class TreeMaybe : MonoBehaviour {

	public GameObject tree;
	Vector3 posTree;
	Quaternion rotTree;

	Quaternion rotRoof;
	Vector3 posRoof;

	private int treeAmount;
	public Transform viewer;
	public GameObject viewerObj;
	public GameObject roof;

	public float view1;
	public float view2;
	public float view3;

	public float chunk1;
	public float chunk2;
	public float chunk3;

	public Vector3 distance;
	public bool hasSpawnedTree;
	public bool hasSpawnedRoof;
	public bool hasSpawnedRocks;

	public GameObject tree01;
	public GameObject tree02;

	public GameObject motherTree;

	public GameObject rock1;
	public GameObject rock2;
	public GameObject rock3;
	public GameObject rock4;
	public GameObject rock5;

	public Vector3 posRock;
	public Quaternion rotRock;

	private int rockAmount;

	public float rng2;







	void Start () {

		//OVERWORLD SPAWNING//

		rotTree = new Quaternion (0, 0, 0, 0);
		posTree = this.transform.localPosition + new Vector3 (0, 1500, 0);
		tree = Resources.Load ("tree 111 1") as GameObject;
		roof = Resources.Load ("Plane (1)") as GameObject;
		tree01 = Resources.Load ("Tree 3_v001") as GameObject;
		tree02 = Resources.Load ("tree_v1") as GameObject;
		motherTree = Resources.Load ("MotherTree") as GameObject;

		rock1 = Resources.Load ("rock1") as GameObject;
		rock2 = Resources.Load ("rock2") as GameObject;
		rock3 = Resources.Load ("rock3") as GameObject;
		rock4 = Resources.Load ("rock4") as GameObject;
		rock5 = Resources.Load ("rock5") as GameObject;

	

		treeAmount = Random.Range (0,2);

		//UNDERWORLD SPAWNING//

		rotRoof = this.transform.rotation;
		posRoof = this.transform.localPosition + new Vector3 (0, 33, 0);

		rotRock = new Quaternion (0, 0, 0, 0);
		posRock = this.transform.localPosition
		+ new Vector3 (Random.Range (-68, 68), Random.Range (2, 30), Random.Range (-68, 68));

		rockAmount = Random.Range (0, 4);
				}




	void Update () {

		Scene scene = SceneManager.GetSceneByName ("mapGenTest");
		Scene scene1 = SceneManager.GetActiveScene ();
	


			if (this.hasSpawnedTree != true) {
				this.hasSpawnedTree = false;
			}

			if (this.hasSpawnedRoof != true) {
				this.hasSpawnedRoof = false;
			}

			if (this.hasSpawnedRocks != true) {
				this.hasSpawnedRocks = false;
			}
			
			viewerObj = GameObject.FindGameObjectWithTag ("viewer");
			viewer = viewerObj.transform;

			view1 = viewer.position.x;
			view2 = viewer.position.z;
			view3 = viewer.position.y;

			chunk1 = this.transform.position.x;
			chunk2 = this.transform.position.z;
			chunk3 = this.transform.position.y;

			distance = new Vector3 (chunk1 - view1, chunk3 - view3, chunk2 - view2);

		if (distance.x <= 108 &&
		     distance.x >= -108 &&
		     distance.z <= 108 &&
		     distance.z >= -108) {
			if (this.hasSpawnedTree == false && scene == scene1) {

				this.hasSpawnedTree = true;

				for (int i = 0; i < treeAmount; i++) {
					Instantiate (tree, posTree, rotTree);
					Instantiate (tree01, posTree, rotTree);
					Instantiate (tree02, posTree, rotTree);

					//Need to create seperate bool for motherTree, I.E, motherTree should only spawn once!
					//Instantiate (motherTree, posTree, rotTree);


				}
			}
			//UNDERWORLD INSTANTIATION//
			if (scene != scene1) {
				if (this.hasSpawnedRoof == false) {
					this.hasSpawnedRoof = true;
					Instantiate (roof, posRoof, rotRoof);

				}
				if (this.hasSpawnedRocks == false) {
					this.hasSpawnedRocks = true;

					for (int x = 0; x < rockAmount; x++) {
						Instantiate (rock1, posRock, rotRock);
						Instantiate (rock2, posRock, rotRock);
						Instantiate (rock3, posRock, rotRock);
						Instantiate (rock4, posRock, rotRock);
						Instantiate (rock5, posRock, rotRock);
					
					}
				}
			}
		}
		


	}
}


