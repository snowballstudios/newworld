﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class moveBro : MonoBehaviour {
	
	public float speed = 6.0F;
	public float jumpSpeed = 8.0F;
	private float gravity = 20.0F;
	private Vector3 moveDirection = Vector3.zero;

	public bool storm;

	public bool NeedsNewPos;
	public Vector3 NewPos;
	    
	public bool jetpack;
	    void Start() {
		storm = false;
		jetpack = false;
	}

		void Update() {

		Scene scene = SceneManager.GetSceneByName ("mapGenTest");
		Scene scene1 = SceneManager.GetActiveScene ();

		if (Input.GetKeyDown (KeyCode.Space)) {
			storm = true;
		}
		if (Input.GetKeyUp (KeyCode.Space)) {
			storm = false;
		}


		if (NeedsNewPos != true) {
			NeedsNewPos = false;
		}

		if (this.transform.position.y <= -2) {
			NeedsNewPos = true;
		} else {
			NeedsNewPos = false;
		}

		if (NeedsNewPos == true) {
			NewPos = new Vector3(Random.Range(-20,20),Random.Range(20,30),Random.Range(-20,20));
			this.transform.position = NewPos;
				} else {
					NewPos = new Vector3(0,0,0);
				}


			CharacterController controller = GetComponent<CharacterController>();
			if (controller.isGrounded) {
				moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
				moveDirection = transform.TransformDirection(moveDirection);
				moveDirection *= speed;
				if (Input.GetButton("Jump"))
					moveDirection.y = jumpSpeed;

				
			}
		if (scene == scene1) {


			if (storm == false) {
				moveDirection.y -= gravity * Time.deltaTime;
			} else {
				moveDirection.y += gravity * Time.deltaTime;
			}
		}
		if (scene != scene1) {
			moveDirection.y -= gravity * Time.deltaTime;
		}
			controller.Move(moveDirection * Time.deltaTime);
		}
	void OnTriggerEnter (Collider c1){
		if (c1.tag == ("Storm")) {
			storm = !storm;
		} 
	}
			
	void OnTriggerExit (Collider c1){
		storm = !storm;
		
	}


	}