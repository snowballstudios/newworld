﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class treeScript : MonoBehaviour {


	public bool terrainCollision = false;
	public Rigidbody rb;
	private Vector3 pos;
	private Quaternion rot;
	public GameObject parentObj;
	public Transform parent;

	public Transform viewer;
	public GameObject viewerObj;
	
	public float view1;
	public float view2;
	public float view3;
	
	public float chunk1;
	public float chunk2;
	public float posY;
	public Vector3 distance;
	public bool treeAct;

	public GameObject tree1;
	public GameObject tree2;
	public GameObject tree3;

	public Vector3 rockPos;

	public GameObject plane;


	// Use this for initialization
	void Start () {


			

		plane = Resources.Load ("Plane (1)") as GameObject;

		Physics.IgnoreCollision(plane.GetComponent<Collider>(), this.GetComponent<Collider>());

		rb = GetComponent<Rigidbody>();
		parentObj = GameObject.FindGameObjectWithTag ("mapGen");
		parent = parentObj.transform;

		this.transform.parent = parent;

		pos = this.transform.localPosition + new Vector3 
			(Random.Range(-139,139),0,Random.Range(-139,139));
		rot = new Quaternion (0,0,0,0);

		rockPos = this.transform.localPosition + new Vector3 (Random.Range(-139,139),Random.Range(2,30),Random.Range(-139,139));

		if (this.gameObject.tag == "tree") {
			this.transform.position = pos;
		} else if (this.gameObject.tag == "rock") {
			this.transform.position = rockPos;
		}
		this.transform.rotation = rot;

		if (this.gameObject.tag == "tree") {
			tree1 = this.transform.FindChild ("Bark_Branch_1").gameObject;
			tree2 = this.transform.FindChild ("Bark_Branch_2").gameObject;
			tree3 = this.transform.FindChild ("Bark_Trunk").gameObject;

		} else if (this.gameObject.tag == "rock") {
			tree1 = this.transform.FindChild ("Default").gameObject;
			tree2 = null;
			tree3 = null;
		}
	}
	
	// Update is called once per frame
	void Update () {

		Scene scene = SceneManager.GetSceneByName ("mapGenTest");
		Scene scene1 = SceneManager.GetActiveScene ();

	
		if (terrainCollision == true && scene != scene1) {
			if (this.transform.position.y <= 2) {
				Destroy (this.gameObject);
				Destroy (tree1);
			}
			rb.constraints = RigidbodyConstraints.FreezeAll;
		}

		if (terrainCollision == true && scene == scene1) {
			if(this.transform.position.y >= 8){
				Destroy (this.gameObject);
				Destroy (tree1);
				Destroy (tree2);
				Destroy (tree3);
			}
		
		}
		// viewerPos + position variables
		viewerObj = GameObject.FindGameObjectWithTag ("viewer");
		viewer = viewerObj.transform;
		
		view1 = viewer.position.x;
		view2 = viewer.position.z;
		view3 = viewer.position.y;
		
		chunk1 = this.transform.position.x;
		chunk2 = this.transform.position.z;
		posY = this.transform.position.y;
		
		distance = new Vector3 (chunk1 - view1, posY - view3, chunk2 - view2);



		RaycastHit hit;
		Vector3 down = transform.TransformDirection (Vector3.down) * 2000;
		
			if (Physics.Raycast (transform.position, (down), out hit)) {
				this.transform.position = new Vector3 (chunk1, posY - hit.distance, chunk2);
			if (this.gameObject.tag != "rock") {
				this.gameObject.layer = 9;
				tree1.layer = 9;
				tree2.layer = 9;
				tree3.layer = 9;
			}
			if (this.gameObject.tag == "rock"){
				this.gameObject.layer = 9;
				tree1.layer = 9;
			}
			}
	}

	void OnCollisionEnter(Collision c){
		terrainCollision = true;

	}
	void OnTriggerEnter(Collider o){
		if (o.gameObject.tag == "tree") {
			Destroy (this.gameObject);

		}
	}
}
