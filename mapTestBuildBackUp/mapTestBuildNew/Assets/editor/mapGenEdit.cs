﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof (mapGen))]
public class mapGenEdit :  Editor {

	public override void OnInspectorGUI(){
		mapGen MapGenerator = (mapGen)target;

		if (DrawDefaultInspector ()) {
			if (MapGenerator.autoUp) {
				MapGenerator.DrawMapInEdit ();
			}
		}


		if (GUILayout.Button ("Generate")) {
			MapGenerator.DrawMapInEdit ();
		}
	}
}
